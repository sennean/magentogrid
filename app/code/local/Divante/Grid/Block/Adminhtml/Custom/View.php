<?php

class Divante_Grid_Block_Adminhtml_Custom_View extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'divante_grid';
        $this->_controller = 'adminhtml_custom_view';
        $this->_headerText = Mage::helper('divante_grid')->__('Divante Custom');

        parent::__construct();
        $this->_removeButton('add');
    }
}