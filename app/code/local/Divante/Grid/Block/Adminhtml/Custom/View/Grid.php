<?php

class Divante_Grid_Block_Adminhtml_Custom_View_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('divante_grid_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _getCollectionClass()
    {
        return 'divante_grid/products_collection';
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('product_id',
            array(
                'header' => $this->__('ID'),
                'align' => 'right',
                'width' => '50px',
                'index' => 'product_id'
            )
        );

        $this->addColumn('name',
            array(
                'header' => $this->__('Nazwa'),
                'index' => 'name'
            )
        );
        $this->addColumn('description',
            array(
                'header' => $this->__('Opis'),
                'index' => 'description'
            )
        );
        $this->addColumn('created_at',
            array(
                'header' => $this->__('Utworzono'),
                'index' => 'created_at'
            )
        );
        $this->addColumn('updated_at',
            array(
                'header' => $this->__('Modyfikowano'),
                'index' => 'updated_at'
            )
        );
        return parent::_prepareColumns();
    }


    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }
}