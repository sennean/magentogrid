<?php
/**
 * Created by PhpStorm.
 * User: karan
 * Date: 08.07.15
 * Time: 23:43
 */

class Divante_Grid_Model_Products extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('divante_grid/products');
    }
}
