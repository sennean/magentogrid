<?php
class Divante_Grid_Model_Mysql4_Products extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('divante_grid/products', 'product_id');
    }
}